#!/bin/bash
MONGODB=bts-mongodb

# https://stackoverflow.com/questions/61486024/mongo-container-with-a-replica-set-with-only-one-node-in-docker-compose
# https://gist.github.com/harveyconnor/518e088bad23a273cae6ba7fc4643549
# https://viblo.asia/p/mongo-replica-set-on-docker-GrLZD773Kk0

echo "Waiting for startup.."
until curl http://${MONGODB}:27017/serverStatus\?text\=1 2>&1 | grep uptime | head -1; do
  printf '.'
  sleep 5
done

echo SETUP.sh time now: `date +"%T" `
mongo --host ${MONGODB}:27017 <<EOF

var cfg = {
    "_id": "rs0",
    "members": [
        {
            "_id": 0,
            "host": "${MONGODB}:27017",
            "priority": 2
        },
    ],settings: {chainingAllowed: true}
};
rs.initiate(cfg, { force: true });
rs.reconfig(cfg, { force: true });
EOF